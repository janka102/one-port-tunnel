const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const http = require("http");
const net = require("net");

const DEFAULT_PORT = 8080;
const DEFAULT_DOMAIN = "localhost";
let { PORT, DOMAIN } = process.env;

const port = PORT || DEFAULT_PORT;
const domain = DOMAIN || DEFAULT_DOMAIN;

const app = express();
const server = net.createServer();
const tunnels = new Map();

let adjectives = [
  "able",
  "best",
  "big",
  "blue",
  "cold",
  "dark",
  "easy",
  "fine",
  "free",
  "full",
  "good",
  "hard",
  "high",
  "hot",
  "huge",
  "left",
  "low",
  "main",
  "new",
  "nice",
  "old",
  "only",
  "open",
  "red",
  "true"
];

const nouns = [
  "air",
  "art",
  "back",
  "book",
  "case",
  "day",
  "door",
  "end",
  "eye",
  "face",
  "fact",
  "game",
  "hand",
  "head",
  "hour",
  "job",
  "law",
  "name",
  "room",
  "side",
  "time",
  "way",
  "week",
  "word",
  "year"
];

const httpServer = http.createServer();

httpServer.on("request", (req, res) => {
  if (!req.headers.host) {
    res.statusCode = 400;
    res.end("Host header is required");
    return;
  }

  let [hostname, reqPort] = req.headers.host.split(":");

  reqPort = reqPort ? parseInt(reqPort, 10) : 80;
  let hostParts = hostname.split(".").reverse();
  let domainParts = domain.split(".").reverse();
  let hostMatches = true;

  for (let i = 0; i < domainParts.length; i++) {
    let domainPart = domainParts[i];
    let hostPart = hostParts.shift();

    if (hostPart !== domainPart) {
      hostMatches = false;
      break;
    }
  }

  if (reqPort !== port || !hostMatches) {
    res.statusCode = 400;
    res.end("Invalid Host");
    return;
  }

  hostParts = hostParts.reverse();

  if (hostParts.length === 0) {
    app(req, res);
    return;
  }

  let subdomain = hostParts.join(".");

  if (!tunnels.has(subdomain)) {
    res.statusCode = 404;
    res.end("Tunnel not found");
    return;
  }

  const tunnel = tunnels.get(subdomain);

  if (tunnel === null) {
    res.statusCode = 400;
    res.end("Tunnel not listening");
    return;
  }

  req.socket.emit("tunnel-forward", subdomain);
});

server.on("connection", socket => {
  socket.once("data", chunk => {
    socket.pause();
    console.log(JSON.stringify(chunk.toString("utf8")));

    if (chunk[0] === 0x06) {
      return listenRequest(socket, chunk);
    }

    socket.tunnelChunks = [chunk];
    socket.unshift(chunk);

    httpServer.emit("connection", socket);

    socket.on("data", chunk => {
      socket.tunnelChunks.push(chunk);
    });

    process.nextTick(() => socket.resume());
  });

  socket.once("tunnel-forward", subdomain => {
    const tunnel = tunnels.get(subdomain);

    while (socket.tunnelChunks.length > 0) {
      tunnel.write(socket.tunnelChunks.shift());
    }

    socket.pipe(tunnel).pipe(socket);
  });
});

function listenRequest(socket, chunk) {
  let subdomain = chunk.slice(1).toString("utf8");
  if (!tunnels.has(subdomain)) {
    socket.end();
    return;
  }

  const tunnel = tunnels.get(subdomain);
  if (tunnel !== null) {
    socket.end();
    return;
  }

  tunnels.set(subdomain, socket);

  socket.on("close", () => {
    tunnels.set(subdomain, null);
  });

  return;
}

app.use(bodyParser.json());
app.use(morgan("dev"));

app.get("/", (req, res) => {
  res.send(
    `
<h1>Welcome to Single Port Tunnel</h1>
Visit <a href="/new">/new</a> to request a new tunnel
`.trim()
  );
});

app.get("/new", (req, res) => {
  let subdomain;

  do {
    subdomain = [
      adjectives[(Math.random() * adjectives.length) | 0],
      nouns[(Math.random() * nouns.length) | 0],
      100 + ((Math.random() * 900) | 0)
    ].join("-");
  } while (tunnels.has(subdomain));

  tunnels.set(subdomain, null);

  res.send(subdomain);
});

server.listen(port, () => {
  let addressInfo = server.address();
  let address;

  if (addressInfo === null) {
    addressInfo = "<unknown>";
  }

  if (typeof addressInfo === "string") {
    address = addressInfo;
  } else {
    address = `localhost:${addressInfo.port}`;
  }

  console.log(`=> Server listening at http://${address}`);
});
