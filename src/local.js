const axios = require("axios");
const net = require("net");

const DEFAULT_REMOTE_PORT = 8080;
const DEFAULT_REMOTE_DOMAIN = "localhost";
const DEFAULT_LOCAL_PORT = 3000;
const DEFAULT_LOCAL_DOMAIN = "localhost";
let { REMOTE_PORT, REMOTE_DOMAIN, LOCAL_PORT, LOCAL_DOMAIN } = process.env;

const remotePort = REMOTE_PORT || DEFAULT_REMOTE_PORT;
const remoteDomain = REMOTE_DOMAIN || DEFAULT_REMOTE_DOMAIN;

const localPort = LOCAL_PORT || DEFAULT_LOCAL_PORT;
const localDomain = LOCAL_DOMAIN || DEFAULT_LOCAL_DOMAIN;

axios
  .get(`http://${remoteDomain}:${remotePort}/new`)
  .then(async res => {
    const subdomain = res.data;
    console.log(`${subdomain}.${remoteDomain}:${remotePort}`);

    while (true) {
      await new Promise((resolve, reject) => {
        const remote = net.connect({
          host: `listen.${subdomain}.${remoteDomain}`,
          port: remotePort
        });

        remote.write(`\x06${subdomain}`);

        remote.setKeepAlive(true);

        remote.on("error", err => {
          console.log("got remote connection error", err.message);
          remote.end();
          reject();
        });

        const connLocal = chunk => {
          console.log("Try local connect");
          remote.pause();

          const local = net.connect({
            host: localDomain,
            port: localPort
          });

          local.once("close", () => {
            console.log("localClose");
            resolve();
          });

          const remoteClose = () => {
            console.log("remoteClose");
            local.end();
          };

          remote.once("close", remoteClose);

          local.once("error", err => {
            local.end();

            remote.removeListener("close", remoteClose);

            if (err.code !== "ECONNREFUSED") {
              return remote.end();
            }

            // retrying
            setTimeout(() => {
              connLocal(chunk);
            }, 1000);
          });

          local.once("connect", () => {
            console.log("Local connected");
            local.write(chunk);
            remote.resume();

            let stream = remote;

            stream.pipe(local).pipe(remote);
          });
        };

        // connect to local when first chunk of data from server comes in
        remote.once("data", chunk => {
          // console.log(chunk.toString("utf8"));
          console.log("Got data from server");
          connLocal(chunk);
        });

        remote.once("connect", () => {
          console.log("remote connect");
        });
      });
    }
  })
  .catch(e => {
    console.log(e.message);
  });
