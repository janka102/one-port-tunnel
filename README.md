# single-port-tunnel

# This is very much a work in progress still

To start the server:

```bash
node src/tunnel.js
```

Then to start the client for tunneling to a local server on port `PPPP`:

```bash
LOCAL_PORT=PPPP node src/local.js
```

That will output something like `hard-time-723.localhost:8080` which you can now use to connect to the server at port `PPPP`:

```bash
hard-time-723.localhost:8080
```

Check out the code for how to specify a different domain and ports with environment variables.

<small>I started testing this out with express and was too lazy to remove it, but this could be done basically without any dependencies Somewhat Easily™ I think</small>
